package com.example.iot.iotroutedetectorjava;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 2/28/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final String TAG = "DatabaseHelper";
    private static final String DB_NAME = "tmb_routes";
/*
    private static final String TABLE_NAME = "people_table";
    private static final String COL1 = "ID";
    private static final String COL2 = "name";
*/


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /* CREATE TABLES */
        String createRoutesTable = "CREATE TABLE `routes` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` VARCHAR(255) NOT NULL)";
        db.execSQL(createRoutesTable);

        String createSequenceTable = "CREATE TABLE `sequence` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `number` INTEGER NOT NULL, `order` INTEGER NOT NULL)";
        db.execSQL(createSequenceTable);

        String createForeignKeySequenceTable = "ALTER table `sequence` ADD COLUMN `route_id` INTEGER REFERENCES `routes`(`id`)";
        db.execSQL(createForeignKeySequenceTable);

        String createStopsTable = "CREATE TABLE `stops` (`id` INTEGER NOT NULL PRIMARY KEY, `name` VARCHAR(255) NOT NULL)";
        db.execSQL(createStopsTable);

        String createForeignKeyStopsTable = "ALTER table `stops` ADD COLUMN `route_id` INTEGER REFERENCES `stops`(`id`)";
        db.execSQL(createForeignKeyStopsTable);


        /* INSERT INTO TABLES */
        // routes
        String insertIntoRoutesTable = "INSERT INTO `routes` (`id`, `name`) VALUES ('1', 'ruta1')";
        db.execSQL(insertIntoRoutesTable);
        String insertIntoRoutesTable2 = "INSERT INTO `routes` (`id`, `name`) VALUES ('2', 'ruta2')";
        db.execSQL(insertIntoRoutesTable2);
        String insertIntoRoutesTable3 = "INSERT INTO `routes` (`id`, `name`) VALUES ('3', 'ruta3')";
        db.execSQL(insertIntoRoutesTable3);

        // sequence
        String insertIntoSequenceTable = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('1', '2', '1', '1')";
        db.execSQL(insertIntoSequenceTable);
        String insertIntoSequenceTable2 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('2', '1', '1', '2')";
        db.execSQL(insertIntoSequenceTable2);
        String insertIntoSequenceTable3 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('3', '3', '1', '3')";
        db.execSQL(insertIntoSequenceTable3);
        String insertIntoSequenceTable4 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('4', '4', '1', '4')";
        db.execSQL(insertIntoSequenceTable4);
        String insertIntoSequenceTable5 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('5', '7', '2', '1')";
        db.execSQL(insertIntoSequenceTable5);
        String insertIntoSequenceTable6 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('6', '3', '2', '2')";
        db.execSQL(insertIntoSequenceTable6);
        String insertIntoSequenceTable7 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('7', '1', '2', '3')";
        db.execSQL(insertIntoSequenceTable7);
        String insertIntoSequenceTable8 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('8', '9', '2', '4')";
        db.execSQL(insertIntoSequenceTable8);
        String insertIntoSequenceTable9 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('9', '4', '3', '1')";
        db.execSQL(insertIntoSequenceTable9);
        String insertIntoSequenceTable10 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('10', '8', '3', '2')";
        db.execSQL(insertIntoSequenceTable10);
        String insertIntoSequenceTable11 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('11', '5', '3', '3')";
        db.execSQL(insertIntoSequenceTable11);
        String insertIntoSequenceTable12 = "INSERT INTO `sequence` (`id`, `number`, `route_id`, `order`) VALUES ('12', '10', '3', '4')";
        db.execSQL(insertIntoSequenceTable12);


        // stops
        String insertIntoStopsTable = "INSERT INTO `stops` (`id`, `name`, `route_id`) VALUES ('1', 'c/maragall', '1')";
        db.execSQL(insertIntoStopsTable);
        String insertIntoStopsTable2 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/balmes', '1')";
        db.execSQL(insertIntoStopsTable2);
        String insertIntoStopsTable3 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/valencia', '1')";
        db.execSQL(insertIntoStopsTable3);
        String insertIntoStopsTable4 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/vallromanes', '1')";
        db.execSQL(insertIntoStopsTable4);
        String insertIntoStopsTable5 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/mallorca', '2')";
        db.execSQL(insertIntoStopsTable5);
        String insertIntoStopsTable6 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/american', '2')";
        db.execSQL(insertIntoStopsTable6);
        String insertIntoStopsTable7 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/singerlin', '2')";
        db.execSQL(insertIntoStopsTable7);
        String insertIntoStopsTable8 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/bac de roda', '2')";
        db.execSQL(insertIntoStopsTable8);
        String insertIntoStopsTable9 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('av. pearson', '3')";
        db.execSQL(insertIntoStopsTable9);
        String insertIntoStopsTable10 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('av. juan carlos III', '3')";
        db.execSQL(insertIntoStopsTable10);
        String insertIntoStopsTable11 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('c/poblenoui', '3')";
        db.execSQL(insertIntoStopsTable11);
        String insertIntoStopsTable12 = "INSERT INTO `stops` (`name`, `route_id`) VALUES ('ronda de la guineueta vella', '3')";
        db.execSQL(insertIntoStopsTable12);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        /*
        db.execSQL("DROP IF TABLE EXISTS " + TABLE_NAME);
        onCreate(db);
        */
    }

    public boolean addData(String item) {
        /*
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2, item);

        Log.d(TAG, "addData: Adding " + item + " to " + TABLE_NAME);

        long result = db.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
        */
        return false;
    }

    /**
     * Returns all the data from database
     * @return
     */
    public Cursor getNumRoutes(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT count(*) FROM `routes`";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getRoutes(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM `routes`";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getSpecificRoute(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM `sequence` WHERE `route_id` = '" + id + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    /**
     * Updates the name field
     * @param newName
     * @param id
     * @param oldName
     */
    public void updateName(String newName, int id, String oldName){

        /*
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME + " SET " + COL2 +
                " = '" + newName + "' WHERE " + COL1 + " = '" + id + "'" +
                " AND " + COL2 + " = '" + oldName + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newName);
        db.execSQL(query);
        */
    }

    /**
     * Delete from database
     * @param id
     * @param name
     */
    public void deleteName(int id, String name){
        /*
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "DELETE FROM " + TABLE_NAME + " WHERE "
                + COL1 + " = '" + id + "'" +
                " AND " + COL2 + " = '" + name + "'";
        Log.d(TAG, "deleteName: query: " + query);
        Log.d(TAG, "deleteName: Deleting " + name + " from database.");
        db.execSQL(query);
        */
    }

}
























