package com.example.iot.iotroutedetectorjava;

import android.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sql.RouteQuerys;

public class MainActivity extends AppCompatActivity {

    RouteQuerys routeQuerys = new RouteQuerys();

    LinearLayout routeButtonLayout;
    DatabaseHelper mDatabaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabaseHelper = new DatabaseHelper(this);

        setContentView(R.layout.activity_main);

        routeButtonLayout = (LinearLayout) findViewById(R.id.routeButtonLayout);

        setRouteButtons(routeButtonLayout);
    }

    private void setRouteButtons(LinearLayout routeButtonLayout){
        Cursor routes  = mDatabaseHelper.getRoutes();

        List<Ruta> routeList = new ArrayList<Ruta>();
        for(routes.moveToFirst(); !routes.isAfterLast(); routes.moveToNext()) {
            // The Cursor is now set to the right position
            Ruta route = new Ruta(routes.getInt(routes.getColumnIndex("id")),
                    routes.getString(routes.getColumnIndex("name")));
            routeList.add(route);
        }

        Log.e("Routes Size",String.valueOf(routeList.size()));

        if(routeList.size() >0){
            for(Ruta r: routeList){
                final Button button = new Button(this);
                button.setId(r._id);
                button.setText("Ruta "+r._id);
                button.setOnClickListener(new OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Cursor secuences = mDatabaseHelper.getSpecificRoute(button.getId());
                        ArrayList<Integer> ruta = new ArrayList<Integer>();
                        List<Secuencia>secuencias = new ArrayList<Secuencia>();
                        for(secuences.moveToFirst(); !secuences.isAfterLast();
                            secuences.moveToNext()){
                            Secuencia sec = new Secuencia(secuences.getInt(secuences.
                                    getColumnIndex("id")),secuences.getInt(
                                    secuences.getColumnIndex("route_id")),secuences.
                                    getInt(secuences.getColumnIndex("number")),
                                    secuences.getInt(secuences.getColumnIndex("order")));
                            secuencias.add(sec);
                        }
                        Toast.makeText(getApplicationContext(), "Analizando ruta "+
                                        String.valueOf(button.getId()),
                                Toast.LENGTH_SHORT).show();
                        for(Secuencia s : secuencias){
                            ruta.add(s.get_number());
                        }
                        startService(v, ruta);
                    }
                });

                routeButtonLayout.addView(button);
            }
        }else{
            TextView noRoutes = new TextView(this);
            noRoutes.setText("No hay rutas disponibles en este momento.");
            routeButtonLayout.addView(noRoutes);
        }
    }

    public void startService(View v, ArrayList<Integer> ruta) {

        Intent serviceIntent = new Intent(this, serviceOnStart.class);
        serviceIntent.putExtra("inputExtra", ruta);

        ContextCompat.startForegroundService(this, serviceIntent);
    }
}
