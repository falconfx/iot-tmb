package com.example.iot.iotroutedetectorjava;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class GetXMLTask extends AsyncTask<String,Void,String> {
    public String id;

    public GetXMLTask(String id) {
        this.id = id;
    }

    @Override
    protected String doInBackground(String... urls) {
        {
            String weather = "UNDEFINED";
            Log.d("Id del simulador", urls[0]);
            try {
                URL url = new URL(urls[0]);
                String string = urls[0];
                URLConnection urlConnection = url.openConnection();

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(urlConnection.getInputStream());
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("devices");
                weather="";
                Node node = nList.item(0);
                if(node.getNodeType()==Node.ELEMENT_NODE){
                    Element el = (Element) node;
                    weather+=el.getElementsByTagName("id").item(0).getTextContent();
                    Log.d("Id del simulador", weather);
                }
            } catch (IOException  | ParserConfigurationException | SAXException e) {
                e.printStackTrace();
            }

            return weather;
        }
    }

    protected void onPostExecute(String data){
        id = data;
    }


}
