package com.example.iot.iotroutedetectorjava;

import android.os.AsyncTask;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class GetEpcTask extends AsyncTask<String,Void,String> {
    public String epc;

    public GetEpcTask(String epc){ this.epc = epc; }

    @Override
    protected String doInBackground(String... urls) {
        String epc = "";
        try {
            URL url = new URL(urls[0]);
            String string = urls[0];
            URLConnection urlConnection = url.openConnection();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(urlConnection.getInputStream());
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("items");
            epc="";
            Node node = nList.item(0);
            if(node.getNodeType()==Node.ELEMENT_NODE){
                Element el = (Element) node;
                try{
                    epc +=el.getElementsByTagName("epc").item(0).getTextContent();
                }catch(Exception e){
                    e.printStackTrace();
                }
                Log.d("Id del simulador", epc);
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        return epc;
    }

    @Override
    protected void onPostExecute(String s) {
        epc = s;
    }
}
