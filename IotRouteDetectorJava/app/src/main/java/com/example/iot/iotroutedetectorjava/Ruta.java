package com.example.iot.iotroutedetectorjava;

public class Ruta {
    int _id;
    String _routeName;

    public Ruta(int _id, String _routeName) {
        this._id = _id;
        this._routeName = _routeName;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_routeName() {
        return _routeName;
    }

    public void set_routeName(String _routeName) {
        this._routeName = _routeName;
    }
}
