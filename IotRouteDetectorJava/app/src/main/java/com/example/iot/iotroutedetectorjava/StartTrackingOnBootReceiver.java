package com.example.iot.iotroutedetectorjava;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class StartTrackingOnBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intnt = new Intent(context, serviceOnStart.class);
        ContextCompat.startForegroundService(context,intnt);
        Log.i("Autostart","Hola Started");
    }
}
