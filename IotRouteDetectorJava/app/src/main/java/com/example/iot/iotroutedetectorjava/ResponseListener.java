package com.example.iot.iotroutedetectorjava;

public interface ResponseListener {
    void onResponseReceive(String data);
}
