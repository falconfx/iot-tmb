package com.example.iot.iotroutedetectorjava;

public class Parada {
    int _id;
    int _routeId;

    public Parada(int _id, int _routeId) {
        this._id = _id;
        this._routeId = _routeId;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int get_routeId() {
        return _routeId;
    }

    public void set_routeId(int _routeId) {
        this._routeId = _routeId;
    }
}
