package com.example.iot.iotroutedetectorjava;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class serviceOnStart extends Service {
    public static final long NOTIFY_INTERVAL = 5* 1000;
    private static final String TAG = "MyService";
    String epc = "";
    String id = "";
    String url = "http://192.168.1.40:3161/devices/";
    private Timer mTimer = null;
    private Handler mHandler = new Handler();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        Log.d(TAG, "onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags,int startid)
    {
        ArrayList<Integer> ruta = (ArrayList<Integer>) intent.getSerializableExtra("inputExtra");

        for(Integer i : ruta){
            System.out.println("Parada "+i);
        }

        try {
            id=new GetXMLTask(id).execute(url).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        url+=id+"/inventory";
        updateDisplay(url, ruta);
        return Service.START_NOT_STICKY;
    }

    private void updateDisplay(final String url, final ArrayList<Integer> ruta) {
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            epc = new GetEpcTask(epc).execute(url).get();
                            Log.d(TAG, epc);

                        } catch (ExecutionException | InterruptedException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (ruta.size() > 0 && (ruta.get(0) == Integer.parseInt(epc))) {
                                ruta.remove(0);
                                Toast.makeText(getApplicationContext(), "Has llegado a la parada "+
                                        String.valueOf(Integer.parseInt(epc)),
                                        Toast.LENGTH_SHORT).show();
                                Log.d("Tamaño stack", String.valueOf(ruta.size()));
                            } else if (ruta.size() == 0) {
                                Toast.makeText(getApplicationContext(), "¿has llegado a tu " +
                                                "destino correctamente!",
                                        Toast.LENGTH_SHORT).show();
                                mTimer.cancel();
                                Intent serviceIntent = new Intent(getApplicationContext(),  serviceOnStart.class);
                                System.out.print("Hola potato");
                                stopService(serviceIntent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        },0,1000);//Update text every second
    }
}
