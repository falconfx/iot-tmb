package com.example.iot.iotroutedetectorjava;

public class Secuencia {
    int _routeId;
    int _id;
    int _number;
    int _order;

    public Secuencia(int _id, int _routeId, int _number, int _order) {
        this._routeId = _routeId;
        this._id = _id;
        this._number = _number;
        this._order = _order;
    }

    public int get_routeId() {
        return _routeId;
    }

    public void set_routeId(int _routeId) {
        this._routeId = _routeId;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int get_number() {
        return _number;
    }

    public void set_number(int _number) {
        this._number = _number;
    }


}
